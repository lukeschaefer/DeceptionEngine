package LevelMaker;

import java.awt.Dialog.ModalityType;
import java.awt.Image;
import java.awt.Label;
import java.awt.Polygon;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.PathIterator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

import Misc.XMLWriter;


public class LevelMaker implements ActionListener {
	static WindowContent	window;
	
	public static void main(String args[]) {
		 try {
		        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		    } catch (Exception e) { }
		LevelMaker lm = new LevelMaker();
	}
	
	public LevelMaker() {
		window = new WindowContent(this);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent source) {
		if (source.getSource() instanceof JMenuItem) {
			JMenuItem item = (JMenuItem) source.getSource();
			switch (item.getText()) {
				case "Load Background...":
					loadBackground();
					break;
				case "Save As...":
					saveAs();
					break;
				case "View Grid":
					window.graphicsPanel.toggleGrid();
					break;
				case "Snap to Grid":
					window.graphicsPanel.toggleSnap();
					break;
				case "Grid Settings":
					showGridMenu();
					System.out.println("Huh?");
					

					break;
				default:
					System.out.println("Wrong Button!");
					
					
			}
		}
		
	}
	
	private String showGridMenu() {

		GridSettings thing = new GridSettings(window);
		return "Test";
		
	}

	private void saveAs() {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Save Level At:");
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = fc.showOpenDialog(window);
		
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			System.out.println("File is: " + file.getAbsolutePath());
			
			// Start writing to the files:
			ArrayList<LevelObject> objects = window.graphicsPanel.objects;
			
			File lightFile = new File(file.getAbsolutePath() + "/lights.txt");
			File wallFile = new File(file.getAbsolutePath() + "/walls.txt");
			
			PrintWriter out;
			try {
				out = new PrintWriter(new FileWriter(lightFile));			
				for (LevelObject object : objects) {
					if (object instanceof Light) {
						Light light = (Light) object;
						String id = Integer.toString((int) (Math.random() * 99999)+100000);
						out.println("light."+id);
						out.println("\tbrightness: "+ Double.toString(Math.random()));
						out.println("\tx: "+ Integer.toString(light.x + light.offsetX));
						out.println("\ty: "+ Integer.toString(light.y + light.offsetY));
						out.println("\tcolor: "+ " #FFCCFF");
					}
				}
				
				out.close();
				out = new PrintWriter(new FileWriter(wallFile));			
				for (LevelObject object : objects) {
					if (object instanceof Zone) {
						Zone zone = (Zone) object;
						PathIterator path = zone.zoneShape.getPathIterator(null);
						System.out.println(((Polygon) zone.zoneShape));						
					
						
						out.println("zone.");
		
					}
			}
			
			out.flush();
			out.close();
			
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			
		}
		
		
	}
	
	private void loadBackground() {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Load Level Background");
		fc.setAcceptAllFileFilterUsed(false);
		fc.addChoosableFileFilter(new ImageFilter());
		int returnVal = fc.showOpenDialog(window);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			Image image;
			try {
				image = ImageIO.read(file);
				window.graphicsPanel.setBackground(image);
			} catch (IOException e) {
				window.alert("Image file could not be read");
			}
			
		}
		
		
	}

	public static void redraw() {
		window.graphicsPanel.repaint();		
	}
	
	
}

class ImageFilter extends FileFilter {
	
	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}
		
		String extension = f.getName().substring(f.getName().indexOf('.') + 1);
		if (extension != null) {
			if (extension.equals("tiff") ||
					extension.equals("tif") ||
					extension.equals("jpeg") ||
					extension.equals("jpg") ||
					extension.equals("gif") ||
					extension.equals("png")) {
				return true;
			} else {
				return false;
			}
		}
		
		return false;
	}
	
	@Override
	public String getDescription() {
		return "Image files (png,tif,jpeg,gif,tiff)";
	}
}
