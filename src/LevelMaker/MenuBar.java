package LevelMaker;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;


public class MenuBar extends JMenuBar {
	
	//Where the GUI is created:
	JMenu					settings, submenu, view;
	JMenuItem				loadBackground, loadFile, saveFile, saveAs, quit;
	JRadioButtonMenuItem	rbMenuItem;
	JCheckBoxMenuItem		viewGrid, snapGrid;
	private JMenuItem gridSettings;
	
	public MenuBar() {
		super();
		
		
		//Settings Menu
			settings = new JMenu("File");
			settings.setMnemonic(KeyEvent.VK_F);
			settings.getAccessibleContext().setAccessibleDescription(
					"The only menu in this program that has menu items");
			add(settings);
			
			loadFile = new JMenuItem("Load File...",
					KeyEvent.VK_L);
			settings.add(loadFile);
			
			loadBackground = new JMenuItem("Load Background...",
					KeyEvent.VK_B);
			settings.add(loadBackground);
			
			saveFile = new JMenuItem("Save File",
					KeyEvent.VK_S);
			settings.add(saveFile);
			
			saveAs = new JMenuItem("Save As...",
					KeyEvent.VK_A);
			settings.add(saveAs);
			
			quit = new JMenuItem("Exit",
					KeyEvent.VK_E);
			settings.add(quit);
			
		// View menu
			view = new JMenu("View");
			view.setMnemonic(KeyEvent.VK_V);
			view.getAccessibleContext().setAccessibleDescription(
					"The only menu in this program that has menu items");
			
			viewGrid = new JCheckBoxMenuItem("View Grid");
			viewGrid.setSelected(true);
			view.add(viewGrid);
			
			snapGrid = new JCheckBoxMenuItem("Snap to Grid");
			snapGrid.setSelected(true);
			view.add(snapGrid);
			
			
			gridSettings = new JMenuItem("Grid Settings");
			view.add(gridSettings);
			
			add(view);
	
		
			
		
	}
	
	public void setListener(ActionListener listener) {
		loadBackground.addActionListener(listener);
		saveAs.addActionListener(listener);
		viewGrid.addActionListener(listener);
		snapGrid.addActionListener(listener);
		gridSettings.addActionListener(listener);
	}
}
