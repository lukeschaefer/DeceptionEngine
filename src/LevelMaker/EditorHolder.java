package LevelMaker;

import javax.swing.JPanel;

public class EditorHolder extends JPanel{
	ObjectEditor editor;
	
	public void setEditor(ObjectEditor editor){
		this.removeAll();
		this.add(editor.editor);
	//	System.out.println("Setting editor to "+editor.obj.getID());
		editor.editor.repaint();
		this.repaint();
	}
}
