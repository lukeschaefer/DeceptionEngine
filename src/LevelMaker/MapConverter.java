package LevelMaker;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.imageio.ImageIO;


public class MapConverter {
	static ArrayList<int[]> foundPixels = new ArrayList<int[]>();
	static ArrayList<Square> squares = new ArrayList<Square>();
	public static void main(String args[]){
		
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File( "../deceptionGame/level_1/mapLOS.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long time = System.nanoTime();
		for(int x = 0; x< image.getWidth()-1; x++){
			System.out.println(   "Row "+(x-1)+" Done."    );
			for(int y = 0; y < image.getHeight()-1; y++){
				int[] pixelTest = {x, y};
				if(image.getRGB( x, y ) == -1 && !alreadyFound(pixelTest)){
					foundPixels.add(pixelTest);
					int i = 1;
					while(x+i<image.getWidth()){
						if(image.getRGB( x+i, y ) == -1)
						{
							int[] pixelTest2 = {x+i, y};
							foundPixels.add(pixelTest2);
						}else{
							break;
						}
						i++;
					}
					int width = i;
					i = 1;
					while(y+i<image.getHeight()){
						int j = 0;
						for(j=0; j<width; j++){
							
							try{
								if(image.getRGB(x+j, y+i) != -1){
									break;
								}
							}catch(ArrayIndexOutOfBoundsException e){
								System.out.println("Error at "+(x+j)+", "+(y+i));
							}
						}
						if(j<width) break;
						
						for(j=0; j<width; j++){
							foundPixels.add(new int[] {x+j,y+i});
						}
						
						i++;
					}
					
					squares.add(new Square(x,y,width,i));
					
				}//else System.out.println("Oops. Already been here...");
			}
		}
		BufferedImage output = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
		 
		PrintWriter fstream;
		try {
			fstream = new PrintWriter(new FileWriter("squareData.txt"));
			fstream.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		for(Square square : squares){
			try{
				fstream = new PrintWriter(new FileWriter("../deceptionGame/level_1/squareData.txt",true));
				fstream.println("Square");	
				fstream.println(square.x);
				fstream.println(square.y);
				fstream.println(square.width);
				fstream.println(square.height);
				fstream.close();	
			}catch (Exception e){//Catch exception if any
			  System.err.println("Error: " + e.getMessage());
			}
			
		
			for(int j = 0; j < square.height; j++)
				for(int i = 0; i < square.width; i++){
					output.setRGB(square.x+i, square.y+j, square.color);
				}
		}
		try {
			ImageIO.write(output, "png", new File("../deceptionGame/level_1/squareData.png"));
			System.out.println("Wrote thing.");
			time = System.nanoTime()-time;
			System.out.println("It took "+time+" seconds to generate "+squares.size()+" walls.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static boolean alreadyFound(int[] pixelMatch){
		for(int[] pixel : foundPixels){
			if(pixel[0] == pixelMatch[0] && pixel[1] == pixelMatch[1]){
				//System.out.println("Already used.");
				return true;
			}
		}
		return false;
	}
	
}

class Square{
	int x;
	int y;
	int width;
	int height;
	int color;
	Square(int x, int y, int width, int height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.color = (int)(Math.random()*10000000);
	}
}