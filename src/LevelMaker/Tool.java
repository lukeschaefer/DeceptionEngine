package LevelMaker;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JRadioButton;
import javax.swing.JToolTip;
import javax.swing.KeyStroke;

public class Tool extends JRadioButton{
	Image toolImage;
	String altText;
	Dimension size;
	boolean selected;
	private ToolTip _tooltip;
	public static Image selectedGlow;
	
	Tool(String altText){
		super();
		_tooltip = new ToolTip( );
		_tooltip.setComponent(this);
		this.altText = altText;
		size = new Dimension(32,28);
		try {
			selectedGlow = ImageIO.read(new File("images/LevelMaker/selectedGlow.png"));
		} catch (IOException e) {
			System.err.println("Could not read the 'selectedGlow.png' file.");
		}
		this.setPreferredSize(size);
		this.setToolTipText(altText);
	}
	
	public JToolTip createToolTip( ) {
		return _tooltip;
	}
	
	public void setKeyToggle(String key){
		
		AbstractAction select = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				
			}
		};
		
		this.getInputMap().put(KeyStroke.getKeyStroke("key"),
                "doSomething");
		this.getActionMap().put("select",
                 select);
		//where anAction is a javax.swing.Action
	}
	
	public void paint(Graphics g){
		g.setColor(this.getBackground());
		g.fillRect(0, 0, 42, 28);
		if(selected){
			g.setColor(this.getBackground().brighter());
			Graphics2D g2 = (Graphics2D)g;
			   g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
			                        RenderingHints.VALUE_ANTIALIAS_ON);
			g2.fillRoundRect(0, 0, (int) size.getWidth(), (int) size.getHeight(), 10, 10);
			g.drawImage(selectedGlow,0, 0, (int) size.getWidth(), (int) size.getHeight(), 0 , 0, selectedGlow.getWidth(null), selectedGlow.getHeight(null),null);
		}
		if(toolImage != null)
			if(selected){
				
				g.drawImage(toolImage, 2, 2, (int) size.getWidth()-2, (int) size.getHeight()-2, toolImage.getWidth(null)/2, 0, toolImage.getWidth(null), toolImage.getHeight(null), null);
			}
			else
				g.drawImage(toolImage, 2, 2, (int) size.getWidth()-2, (int) size.getHeight()-2,0, 0, toolImage.getWidth(null)/2, (int) toolImage.getHeight(null), null);
		//g.setColor((this.getBackground().darker()));
		//g.drawRect(0,0,31,27);
	}

	public void setImage(String fileName) {
		try {
			this.toolImage = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			System.err.println("Error loading image for tool: "+altText+".\n\t Image name is: "+fileName);
		}
		
	}
}
