package LevelMaker;

import java.util.ArrayList;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Rectangle;

import java.io.*;

import org.newdawn.slick.imageout.ImageOut;
import org.newdawn.slick.imageout.ImageWriter;


public class TileMap implements Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6442967599948396624L;
	
	private Tile[][] tileArray;
	int imageSize = 64;

	int width;
	int height;
	
	public TileMap(int width, int height) {
		tileArray = new Tile[width][height];
		for(int x = 0; x<tileArray.length;x++)
			for(int y=0; y<tileArray[x].length;y++){
				tileArray[x][y]=new Tile(x,y);
				
			}
		this.width = width;
		this.height = height;
	}

	public TileMap(){
		
	}
	

	
	public void save(){
		System.out.println("SAVING IMAGE!");
		
		//images
		Image left;
		Image right;
		Image square;
		Image squareTop;
		Image background;
		try{
			left = new Image("blocks/left.png");
			right = new Image("blocks/right.png");
			square = new Image("blocks/square.png");
			squareTop = new Image("blocks/squareTop.png");
			background = new Image("blocks/background.png");
			
		
		Image imageG;
		Graphics g;
	
		imageG = new Image((width+1)*imageSize, (height+1)*imageSize);
		 g = imageG.getGraphics();
		
		 for(int x = 0; x< imageG.getWidth(); x+=512)
			 for(int y = 0; y< imageG.getHeight(); y+=512)
				 g.drawImage(background, x, y);
				
		
		for(int x = 0; x<tileArray.length; x++){
			for(int y = 0; y<tileArray[x].length; y++){
			
				switch(tileArray[x][y].tileMode){
				case Tile.BOX_EMPTY : break;
				case Tile.BOX_SQUARE : g.drawImage(square,x*imageSize, y*imageSize);
										break;
				case Tile.BOX_LEFT : g.drawImage(left,x*imageSize, y*imageSize);
									break;
				case Tile.BOX_RIGHT : g.drawImage(right,x*imageSize, y*imageSize);
										break;
				default :	break;
				}
			}
		}
		g.flush();
		ImageOut.write(imageG, "png",  new FileOutputStream(new File("levelImage.png")), false);
		
		ObjectOutput out = new ObjectOutputStream(new FileOutputStream("tileMap.ser"));
	    out.writeObject(this);
	    out.close();
	    
		System.out.println("DONE SAVING IMAGE!");
		}catch(SlickException | IOException e){
		System.err.println("Error loading images for saving.");
		
		}
	}
	public Tile[][] getTileArray(){
		return tileArray;
	}
	
	public ArrayList<Tile> getBoxList() {
		ArrayList<Tile> returnList = new ArrayList<Tile>();
		for(Tile[] tileArray2 : tileArray)
			for(Tile tile : tileArray2)
				returnList.add(tile);
		return returnList;
	}
	
	public static TileMap deserialize(String filename){
		// Deserialize from a file
	    
	  
		try {
			  TileMap tilemap;
			File file = new File(filename);
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
			tilemap = (TileMap) in.readObject();
			in.close();
			return tilemap;
		} catch (ClassNotFoundException | IOException e) {
			System.err.println("NOT WORK!");
			e.getMessage();
			e.printStackTrace();
			return new TileMap(1,1);
		}
	    

	    
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}