package LevelMaker;

import java.awt.Graphics;
import java.awt.Shape;


public class LevelObject {
	
	boolean		selected	= false;
	String id 		= "";
	Shape		clickBox;
	public int	offsetX;
	public int	offsetY;
	
	public void draw(int cameraX, int cameraY, Graphics g) {}
	
	/** Set whether the object is selected
	 * 
	 * @param isSelected */
	public void setSelected(boolean isSelected) {
		this.selected = isSelected;
		
	}
	
	public Shape getClickZone() {
		return clickBox;
	}

	public String getID() {
		// TODO Auto-generated method stub
		return id;
	}
	
}
