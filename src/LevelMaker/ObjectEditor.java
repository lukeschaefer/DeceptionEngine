package LevelMaker;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ObjectEditor implements ActionListener{
	public LevelObject obj;
	public JPanel editor;
	
	private int mode;
	private JComboBox<String> changeZone;
	private JTextField zoneHeight;
	private JTextField lightFlicker;	
		private static final int ZONE = 0;
		private static final int LIGHT = 1;
		
	public ObjectEditor(LevelObject obj){
		this.obj = obj;
		editor = new JPanel();
		
		if(obj instanceof Zone){
			this.mode = ZONE;
			zoneSetup();
		}
		else if(obj instanceof Light){
			this.mode = LIGHT;
			lightSetup();
		}
		editor.repaint();
	}

	private void lightSetup() {		
		editor.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.fill = gbc.HORIZONTAL;		
		
		gbc.gridy = 1;
		lightFlicker = new JTextField("1");
		lightFlicker.setToolTipText("Set flicker with a string of letters");
		
		editor.add(new Label("Light flicker:"), gbc);
		editor.add(lightFlicker, gbc);

	}

	private void zoneSetup() {
		editor.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		String[] modeList = { "Both", "Objects Only", "Light Only" };
		changeZone = new JComboBox<String>(modeList);
		
		editor.add(new Label("Zone Affects: "), gbc);
		editor.add(changeZone, gbc);
		changeZone.addActionListener(this);
		
		gbc.gridy = 1;
		zoneHeight = new JTextField("1");
		zoneHeight.setToolTipText("Set height from 0 to 1. Used for shadow lengths and cover. 1 is 'infinite' ");
		
		editor.add(new Label("Zone Height:"), gbc);
		editor.add(zoneHeight, gbc);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(mode == ZONE)
			if(e.getSource() == changeZone)
				((Zone) obj).type = changeZone.getSelectedIndex();
		LevelMaker.window.graphicsPanel.repaint();
	
	}
	
	
	
	
}
