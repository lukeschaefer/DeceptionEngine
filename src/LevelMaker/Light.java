package LevelMaker;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;


public class Light extends LevelObject {
	int	x;
	int	y;

	
	public Light(int x, int y, int cameraX, int cameraY) {
		this.offsetX = cameraX;
		this.offsetY = cameraY;
		this.x = x;
		this.y = y;
		this.clickBox = new Rectangle(x - 1, y - 1, 7, 7);
		this.id = "light."+(int)(Math.random()*10000);

	}
	

	
	@Override
	public void draw(int cameraX, int cameraY, Graphics g) {
		
		if (selected)
			g.setColor(Color.yellow);
		else
			g.setColor(Color.white);
		Graphics2D g2d = (Graphics2D) g;
		g2d.translate(-(cameraX - offsetX), -(cameraY - offsetY));
		g2d.setRenderingHint(
				RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.fillOval(x - 1, y - 1, 7, 7);
		if (selected)
			g.setColor(Color.gray);
		else
			g.setColor(Color.black);
		g2d.drawOval(x - 1, y - 1, 7, 7);
		g2d.translate((cameraX - offsetX), (cameraY - offsetY));
		
	}
}
