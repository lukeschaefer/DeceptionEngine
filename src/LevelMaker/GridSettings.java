package LevelMaker;

import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Dialog.ModalityType;

import javax.swing.JDialog;

public class GridSettings extends JDialog{

	public GridSettings(Frame owner){
		super(owner);
		setAlwaysOnTop(true);
		setAutoRequestFocus(true);
		setEnabled(true);
		setModal(true);
		setModalityType(ModalityType.APPLICATION_MODAL);
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		this.getContentPane().setSize(500,500);
		
		gbc.fill = gbc.HORIZONTAL;
		gbc.insets = new Insets(5,5,5,5);
		gbc.gridx = 0;
		gbc.gridy = 0;
		
			this.add(new Label("Show Grid"),gbc);
		
		gbc.gridx = 1;
			
			this.add(new Label("Grid Density"),gbc);
		
		gbc.gridy = 1;
		gbc.gridx = 0;
			
			this.add(new Label("Snap to Grid"),gbc);
		
		gbc.gridx = 1;
			
			this.add(new Label("Grid Color"),gbc);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 2;
		
			this.add(new Label("Grid Opacity"),gbc);
		
		resize(500,500);
		show();
		
	}
}
