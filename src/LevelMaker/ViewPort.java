package LevelMaker;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import javax.swing.JPanel;


public class ViewPort extends JPanel implements MouseMotionListener, MouseListener {
	Image							levelBackground;
	
	Point							spawnLocation;
	
	
	Shape							currentShape;
	LevelObject						selectedObject;
	
	int[] 							startLocation;
	
	ArrayList<ZoneChangeListener>	listeners;
	
	ArrayList<Point>				polyPoints;
	
	WindowContent					window;
	int								mouseX	= 0;
	int								mouseY	= 0;
	
	boolean							spaceDown;
	
	int								cameraX	= 0;
	int								cameraY	= 0;
	
	ArrayList<LevelObject>			objects;
	
	private final boolean[]			keys;

	private boolean 				grid = true;
	private int						gridWidth = 20;
	private boolean 				snap = true;
	
	public ViewPort(WindowContent window) {
		super();
		this.window = window;
		addMouseMotionListener(this);
		addMouseListener(this);
		objects = new ArrayList<LevelObject>();
		polyPoints = new ArrayList<Point>();
		listeners = new ArrayList<ZoneChangeListener>();
		spawnLocation = new Point(0, 0);
		keys = new boolean[525];
	}
	
	
	@Override
	public void paint(Graphics g) {
		
		// Draw Background.
		g.setColor(new Color(50,45,40));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		if (levelBackground != null)
			g.drawImage(levelBackground, 0, 0, this.getWidth(), this.getHeight(), cameraX, cameraY, cameraX + this.getWidth(), cameraY
					+ this.getHeight(), null);
		
		//Draw Grid
		if(grid){
			Graphics2D g2 = (Graphics2D) g;			
			g2.setColor(new Color(255,220,200,30));
			g2.setStroke(new BasicStroke(2f));	
			
			for(int x = gridWidth-(cameraX%gridWidth); x < this.getWidth(); x += gridWidth){
						
				g2.drawLine(x, 0, x, this.getHeight());
			}
			for(int y = gridWidth-(cameraY%gridWidth); y < this.getHeight(); y += gridWidth){				
				g2.drawLine(0, y, this.getWidth(), y);
			}
			
			g2.setStroke(new BasicStroke(1f));	
			g.setColor(new Color(0,0,0));
			g2.setRenderingHint(
			        RenderingHints.KEY_TEXT_ANTIALIASING,
			        RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		}
		
		
		//Draw Zones.
		for (LevelObject object : objects) {
			//Set color for zone drawing
			object.draw(cameraX, cameraY, g);
		}
		
		//Draw currently forming shape (when using rect/circle/poly tool)
		if (currentShape != null) {
			g.setColor(new Color(150, 150, 150));
			Graphics2D gTest = (Graphics2D) g;
			gTest.draw(currentShape);
			gTest.setColor(new Color(150, 150, 150, 100));
			gTest.fill(currentShape);
		}
		
		//Draw current Points (poly tool)
		if (polyPoints.size() > 0) {
			g.setColor(Color.red);
			for (Point point : polyPoints) {
				g.drawRect(point.x - 2, point.y - 2, 4, 4);
				g.setColor(Color.white);
			}
		}
		if (this.spawnLocation != null) {
			g.setColor(Color.blue);
			g.drawOval(spawnLocation.x - 6 - cameraX, spawnLocation.y - 6 - cameraY, 12, 12);
		}
	}
	
	
	public void setBackground(Image bkg) {
		this.levelBackground = bkg;
		this.repaint();
		
	}
	
	
	@Override
	public void mouseDragged(MouseEvent e) {
			mouseX = e.getX();
			mouseY = e.getY();
		if(snap){
			snapMouse();
		}
	
		if (currentTool() == "Move") {
		
			cameraX = startLocation[0] - e.getX();
			cameraY = startLocation[1] - e.getY();			
		}
		
		else if (currentTool() == "Rectangle") {
			if (currentShape == null) {
				currentShape = new Rectangle(mouseX, mouseY, 0, 0);
				startLocation = new int[] {mouseX, mouseY};
			} else {				
				int realStartX = 	Math.min(startLocation[0], mouseX);
				int realEndX = 		Math.abs(realStartX-Math.max(startLocation[0], mouseX));
				
				int realStartY = 	Math.min(startLocation[1], mouseY);
				int realEndY = 		Math.abs(realStartY-Math.max(startLocation[1], mouseY));
				currentShape = new Rectangle(realStartX, realStartY, realEndX, realEndY);
			}
		}
		
		else if (currentTool() == "Select" && selectedObject != null) {	

			if(startLocation == null){
				startLocation = new int[] {mouseX, mouseY};
			}
			selectedObject.offsetX += mouseX - startLocation[0];
			selectedObject.offsetY += mouseY - startLocation[1];
			
			startLocation[0]=mouseX;
			startLocation[1] = mouseY;
		}
		
		else if (currentTool() == "Circle") {
			if (currentShape == null) {
				currentShape = new Ellipse2D.Float(mouseX, mouseY, 0, 0);
				startLocation = new int[] {mouseX, mouseY};
			} else {				
				int realStartX = 	Math.min(startLocation[0], mouseX);
				int realEndX = 		Math.abs(realStartX-Math.max(startLocation[0], mouseX));
				
				int realStartY = 	Math.min(startLocation[1], mouseY);
				int realEndY = 		Math.abs(realStartY-Math.max(startLocation[1], mouseY));
				currentShape = new Ellipse2D.Float(realStartX, realStartY, realEndX, realEndY);
			}			
		}
		
		
		repaint();
			
	}
	
	
	@Override
	public void mouseMoved(MouseEvent e) {
			mouseX = e.getX();
			mouseY = e.getY();
		if(snap){
			snapMouse();
		}
		
		if (currentTool() == "Move") {
			this.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} else
			this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

	}
	
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// Snap dat nigga.
		
			mouseX = e.getX();
			mouseY = e.getY();
		if(snap){
			snapMouse();
		}
		
		//Unselect any selected zones.
		
		for (LevelObject object : objects)
			object.setSelected(false);
		selectedObject = null;
		
		// Select a zone.
		if (currentTool() == "Select") {
			for (LevelObject object : objects) {
				if (object.getClickZone().contains(e.getX() + cameraX - object.offsetX, e.getY() + cameraY - object.offsetY)) {
					selectZone(object);
					break;
				}
			}
			repaint();
		}
		
		if (currentTool() == "Polygon") {
			if (polyPoints.size() > 0 && polyPoints.get(0).distance(mouseX, mouseY) < 4) {
				int[] xPoints = new int[polyPoints.size()];
				int[] yPoints = new int[polyPoints.size()];
				int i = 0;
				for (Point point : polyPoints) {
					xPoints[i] = point.x;
					yPoints[i] = point.y;
					i++;
				}
				Zone newPolyZone = new Zone(new Polygon(xPoints, yPoints, xPoints.length), cameraX, cameraY);
				
				objects.add(newPolyZone);
				polyPoints.clear();
				selectZone(newPolyZone);
			} else {
				polyPoints.add(new Point(mouseX, mouseY));
			}
			repaint();
		}
		
		if (currentTool() == "Light") {
			System.out.println("You'd best watch yo'self nigga. We be lightin up in deze streets.");
			Light newLight = new Light(mouseX, mouseY, cameraX, cameraY);
			
			objects.add(newLight);
			selectZone(newLight);
			repaint();
		}
		
		if (currentTool() == "Delete") {
			for (LevelObject object : objects) {
				if (object.getClickZone().contains(mouseX + cameraX - object.offsetX, mouseY + cameraY - object.offsetY)) {
					objects.remove(object);
					break;
				}
			}
			repaint();
		}
		
		
	}
	
	private void snapMouse() {
		mouseX= gridWidth*(int)((mouseX-10)/20)+gridWidth-(cameraX%gridWidth);
		mouseY= gridWidth*(int)((mouseY-10)/20)+gridWidth-(cameraY%gridWidth);
	}


	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(currentTool() == "Move"){
			startLocation = new int[] {e.getX()+cameraX, e.getY()+cameraY};
		}
		
		if (currentTool() == "Select") {
			if(selectedObject != null)
				if(selectedObject.getClickZone().contains(e.getX() + cameraX - selectedObject.offsetX, e.getY() + cameraY - selectedObject.offsetY)){
					
					System.out.println("I ain't gonna switch nothing nigga!");
					return;
				}
			
			for (LevelObject object : objects) {
				if (object.getClickZone().contains(e.getX() + cameraX - object.offsetX, e.getY() + cameraY - object.offsetY)) {
					selectZone(object);
					repaint();	
					break;
				}
			}
			repaint();
		}
		
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if (currentTool() == "Circle") {
			if (currentShape != null) {
				Zone zone = new Zone((currentShape), cameraX, cameraY);
				objects.add(zone);
				selectZone(zone);
				currentShape = null;
				repaint();
				
			}
		}
		if (currentTool() == "Rectangle") {
			if (currentShape != null) {
				Zone zone = new Zone(rectangleToPolygon((Rectangle) currentShape), cameraX, cameraY);
				objects.add(zone);
				currentShape = null;
				selectZone(zone);
				repaint();
				
			}
		}
		startLocation = null;
		
	}
	
	
	private void selectZone(LevelObject obj) {
		this.selectedObject = obj;
		for (LevelObject object : objects) {
			object.setSelected(false);
			if (object == obj) {
				obj.setSelected(true);
				this.selectedObject = obj;
			}
		}
		//swaps it out to the top of the list.
		objects.set(objects.indexOf(obj), objects.get(objects.size() - 1));
		objects.set(objects.size() - 1, obj);
		alertListeners(obj);
		
	}
	
	
	private void alertListeners(LevelObject obj) {	
		System.out.println("Changing selection to "+obj.getID());	
		LevelMaker.window.bottomButtons.setEditor(new ObjectEditor(obj));
	}
	
	public static Polygon rectangleToPolygon(Rectangle rect)
	{
		Polygon result = new Polygon();
		result.addPoint(rect.x, rect.y);
		result.addPoint(rect.x + rect.width, rect.y);
		result.addPoint(rect.x + rect.width, rect.y + rect.height);
		result.addPoint(rect.x, rect.y + rect.height);
		return result;
	}
	
	
	public String currentTool() {
		return window.zoneTools.selectedTool.altText;
	}
	
	public void addZoneChangeListener(ZoneChangeListener z) {
		listeners.add(z);
		
	}


	public void toggleGrid() {
		System.out.println("Toggling grid to "+!grid+", nigga!");
		grid = !grid;
		this.repaint();
	}


	public void toggleSnap() {
		System.out.println("Toggling snap to "+!snap);
		snap = !snap;
		
	}
	
	
}
