package LevelMaker;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;

public class ToolBox extends JPanel implements ActionListener{

	private ButtonGroup group;	
	public Tool selectedTool;
	private GridBagConstraints gbc;
	private ArrayList<Tool> toolList;
	
	private Color backgroundColor;
	
	public ToolBox(){
	    
		toolList = new ArrayList<Tool>();
		backgroundColor = new Color(255,255,255);
	    this.setLayout(new GridBagLayout());
	    
	    gbc = new GridBagConstraints();
	    
	    group = new ButtonGroup();


	    gbc.weightx = 1;
	    gbc.weighty = 1;
	    gbc.fill = GridBagConstraints.BOTH;
	    this.selectedTool = new Tool("NULL");

	}

	public void addTool(String toolName, String toolImageName){
		 gbc.weightx = 1;
		 gbc.weighty = 1;
		 gbc.insets = new Insets(2,2,2,2);
		 gbc.fill = GridBagConstraints.BOTH;
		 
		Tool newTool = new Tool(toolName);
		newTool.setBackground(backgroundColor);
		newTool.setImage(toolImageName);
		newTool.addActionListener(this);
	    group.add(newTool);
	    gbc.gridy++;
	    this.add(newTool ,gbc);
	    toolList.add(newTool);
	}
	
	public void addBreak() {
		JPanel breakPanel = new JPanel();
		breakPanel.setPreferredSize(new Dimension(20,2));
		breakPanel.setBackground(new Color(75,75,75));
		gbc.weightx = 1;
		gbc.weighty = .1;
		gbc.insets = new Insets(0,2,0,2);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridy++;
		this.add(breakPanel,gbc);
		
	}
	
	public void setBackground(Color bg){
		super.setBackground(bg);
		this.backgroundColor = bg;
	}
	public void selectTool(Tool toolname){
		for(Tool deselect : toolList)
			deselect.selected = false;
		toolname.selected = true;
		this.selectedTool = toolname;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		selectTool((Tool) e.getSource());
	}

	

}
