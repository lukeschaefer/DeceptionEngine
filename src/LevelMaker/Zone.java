package LevelMaker;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;


public class Zone extends LevelObject {
	Shape	zoneShape;
	
	public Zone(Shape currentShape, int cameraX, int cameraY) {
		this.type = BOTH;
		zoneShape = currentShape;
		this.clickBox = zoneShape;
		
		offsetX = cameraX;
		offsetY = cameraY;
		
		this.id = "rect."+(int)(Math.random()*10000);
		
	}
	
	int					type;
	static final int	BOTH		= 0;
	static final int	LIGHTBLOCK	= 1;
	static final int	OBJBLOCK	= 2;
	
	@Override
	public void setSelected(boolean b) {
		this.selected = b;
		
	}
	
	@Override
	public void draw(int cameraX, int cameraY, Graphics g) {
		
		Graphics2D g2d = (Graphics2D) g;
		
		// Translate graphics context to where the shape is:
		g2d.translate(-(cameraX - offsetX), -(cameraY - offsetY));
		
		// if Selected, draw a white border.
		
		if (selected) {
			g2d.setColor(new Color(255, 255, 255));
			g2d.draw(this.zoneShape);
		}
		
		//for the insides (outsides if not selected, get the appropriate color
		switch (this.type) {
			case Zone.BOTH:
				g.setColor(new Color(150, 150, 150));
				break;
			case Zone.LIGHTBLOCK:
				g.setColor(new Color(200, 100, 100));
				break;
			case Zone.OBJBLOCK:
				g.setColor(new Color(200, 200, 40));
		}
		
		// Draw border if not selected
		if (!selected) {
			g2d.draw(this.zoneShape);
		}
		
		// Turn the color translucent for fill drawing:
		g2d.setColor(new Color(g.getColor().getRed(), g.getColor().getGreen(), g.getColor().getBlue(), 50));
		g2d.fill(this.zoneShape);
		g2d.translate((cameraX - offsetX), (cameraY - offsetY));
		
		
	}
}
