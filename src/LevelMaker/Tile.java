package LevelMaker;
import java.io.Serializable;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Rectangle;


public class Tile implements Serializable{
	int x;
	int y;
	boolean highLight = false;
	Polygon leftBox;
	Polygon rightBox;
	
	
	int tileMode;
	public static final int BOX_EMPTY = 0;
	public static final int BOX_SQUARE = 1;
	public static final int BOX_RIGHT = 2;
	public static final int BOX_LEFT = 3;
	private static final int BOX_MODECOUNT = 4;
	
	public Tile(int x, int y){
		this.x = x;
		this.y = y;		
		
		
		}
	
	


	public void highLight() {
		this.highLight = true;
		
	}



	public void switchMode(int button) {
		if(button == 0) button = -1;
		tileMode -= button;
		if(tileMode == -1)
			tileMode = Tile.BOX_MODECOUNT-1;
		if(tileMode == Tile.BOX_MODECOUNT)
			tileMode = 0;
		
	}
	
	public void setMode(int mode) {
		tileMode = mode;		
	}


	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
}
