package LevelMaker;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


class WindowContent extends JFrame {
	
	ViewPort	graphicsPanel;
	JPanel		rightButtons;
	EditorHolder		bottomButtons;
	MenuBar		menuBar;
	ToolBox		zoneTools;
	ToolBox		spawnTools;
	Color backgroundColor;
	
	public WindowContent(LevelMaker levelMaker) {
		super();
		this.backgroundColor = new Color(220,217,210);
		this.setTitle("Level Maker v0.4");
		
		this.setFocusable(true);
		java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(50, 50);
		this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
		JPanel content = new JPanel();
		content.setLayout(new BorderLayout());
		
		
		GridBagConstraints gbc = new GridBagConstraints();
		content.setBorder(new EmptyBorder(3, 3, 3, 3));
		menuBar = new MenuBar();
		menuBar.setListener(levelMaker);
		// Graphics Panel:
		gbc.weightx = 2;
		gbc.insets = new Insets(10, 10, 10, 0);
		gbc.weighty = 2;
		gbc.gridx = 0;
		gbc.gridy = 0;
		
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridwidth = 3;
		graphicsPanel = new ViewPort(this);
		content.add(graphicsPanel, BorderLayout.CENTER);
		
		// Buttons on right
		gbc.insets = new Insets(10, 10, 10, 10);
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridx = 4;
		gbc.gridwidth = 1;
		rightButtons = new JPanel();
		rightButtons.setPreferredSize(new Dimension(40, 100));
		zoneTools = new ToolBox();
		zoneTools.setBackground(backgroundColor);
		zoneTools.addTool("Select", "images/LevelMaker/selectTool.png");
		zoneTools.addTool("Move", "images/LevelMaker/handTool.png");
		zoneTools.addBreak();
		zoneTools.addTool("Rectangle", "images/LevelMaker/rectTool.png");
		zoneTools.addTool("Circle", "images/LevelMaker/circleTool.png");
		zoneTools.addTool("Polygon", "images/LevelMaker/polyTool.png");
		zoneTools.addBreak();
		zoneTools.addTool("Spawn", "images/LevelMaker/spawnTool.png");
		zoneTools.addTool("Light", "images/LevelMaker/lightTool.png");
		zoneTools.addTool("Delete", "images/LevelMaker/trashTool.png");
		rightButtons.add(zoneTools);
		content.add(rightButtons, BorderLayout.EAST);
		
		// Buttons on bottom
		gbc.gridx = 0;
		gbc.gridwidth = 5;
		gbc.gridy = 4;
		bottomButtons = new EditorHolder();
		bottomButtons.setPreferredSize(new Dimension(200, 200));

		content.add(bottomButtons, BorderLayout.SOUTH);
		
		
		graphicsPanel.setBackground(Color.red);
		rightButtons.setBackground(backgroundColor );
		bottomButtons.setBackground(backgroundColor);
		
		this.setSize(900, 700);
		content.setBackground(backgroundColor);
		
		this.setJMenuBar(menuBar);
		this.getContentPane().add(content);
		this.setVisible(true);
	}
	
	public void alert(String string) {
		// put popup here later;
		System.out.println(string);
		
	}
	
}