package GUI;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;


/** @author Luke */
public class GUILayout {
	int				x;
	int				y;
	
	int				width;
	int				height;
	
	int				xSpaces;
	int				ySpaces;
	
	int				cellWidth;
	int				cellHeight;
	
	Input			input;
	
	GUIListener		listener;
	GUIElement[][]	gridParts;
	
	
	/*** @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param xSpaces
	 * @param ySpaces
	 * @param listener */
	public GUILayout(int x, int y, int width, int height, int xSpaces, int ySpaces, GUIListener listener) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.xSpaces = xSpaces;
		this.ySpaces = ySpaces;
		this.listener = listener;
		this.cellHeight = height / ySpaces;
		this.cellWidth = width / xSpaces;
		gridParts = new GUIElement[height][width];
		this.input = listener.getInput();
	}
	
	public void add(GUIElement part, int x, int y) {
		gridParts[x][y] = part;
		part.putInCell(new Cell((x - 1) * cellWidth + this.x, (y - 1) * cellHeight + this.y, cellWidth, cellHeight));
		part.thisLayout = this;
		listener.getInput().addMouseListener(part);
	}
	
	public void draw(Graphics g) {
		for (int y = 0; y < gridParts.length; y++)
			for (int x = 0; x < gridParts[y].length; x++)
				if (gridParts[y][x] != null)
					gridParts[y][x].drawGUI(g);
	}
	
	public int[] getAbsolutePosition(int[] slotPosition) {
		int x = this.x + (this.width / xSpaces) * slotPosition[0];
		int y = this.y + (this.height / ySpaces) * slotPosition[1];
		int width = (this.width / xSpaces) * slotPosition[2];
		int height = (this.height / ySpaces) * slotPosition[3];
		return new int[] { x, y, width, height };
	}
	
	public void destroy() {
		
	}
	
	public void removeListeners() {
		for (GUIElement[] array : gridParts)
			for (GUIElement part : array)
				input.removeMouseListener(part);
		
	}
	
	
}
