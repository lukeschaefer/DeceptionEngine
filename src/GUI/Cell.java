package GUI;

public class Cell {
	
	public int width;
	public int height;
	public int x;
	public int y;
	public GUIElement element;
	
	
	public Cell(int x, int y, int width, int height) {
		super();
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;

	}


}
