package GUI;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;

public interface GUIListener {
		public Input getInput();
         public void guiActionPreformed(GUIEvent e);
 
         
}