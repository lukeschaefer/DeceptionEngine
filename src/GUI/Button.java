package GUI;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.MouseListener;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

import Engine.ShadowCaster;


public class Button extends GUIElement{
	static final int NORMAL = 0;
	static final int HOVER = 1;
	private int mode;
	
	String text;
	
	
	public Button(String text) {
		width = 20+text.length()*10;
		height = 30;
		
		this.text = text;
		this.clickBox = new Rectangle(this.x,this.y,width,height);
		mode = Button.NORMAL;
		
	}
	
	public Button(String text, int x, int y) {
		width = 20+text.length()*10;
		height = 30;
		
		this.x = x-width/2;
		this.y = y-height/2;
		
		this.text = text;
		this.clickBox = new Rectangle(this.x,this.y,width,height);
		mode = Button.NORMAL;
	}
	
	public Button(String text, GUILayout thisLayout) {
		this.x = thisLayout.getAbsolutePosition(slotPosition)[0];
		this.y = thisLayout.getAbsolutePosition(slotPosition)[1];
		this.width = thisLayout.getAbsolutePosition(slotPosition)[2];
		this.height = thisLayout.getAbsolutePosition(slotPosition)[3];
		this.thisLayout = thisLayout;
		this.text = text;
		this.clickBox = new Rectangle(this.x,this.y,width,height);
		mode = Button.NORMAL;
	}


	
	public void drawGUI(Graphics g) {		
		g.setDrawMode(Graphics.MODE_NORMAL);
		if(getMode() == HOVER)
			g.setColor(new Color(200,200,200));	
		else
			g.setColor(new Color(100,100,100));
		g.fillRect(cell.x,cell.y,cell.width,cell.height);
		g.setColor(Color.white);	
		
		g.drawString(text, (cell.x+cell.width/2)-g.getFont().getWidth(text)/2, cell.y+cell.height/2-g.getFont().getHeight("P")/2);
		
	}
	
	public void draw(Graphics g){
		g.setDrawMode(Graphics.MODE_NORMAL);
		
		g.setColor(new Color(200,200,200));	
		g.fillRect(x,y,cell.width,cell.height);
		g.setColor(Color.white);
		//g.drawString(text, x+10, y+5);
	}


	public void fireEvent(){
		this.thisLayout.listener.guiActionPreformed(new GUIEvent(this));
	}
	
	
	public void mouseMoved(int oldx, int oldy, int newx, int newy){
	
		if(clickBox.contains(newx, newy)){
			setMode(Button.HOVER);
		}
		else
			setMode(Button.NORMAL);
	}

	private void setMode(int mode) {
		this.mode = mode;
		
	}

	public int getMode() {
		return mode;
	}

	public String getText() {
		return text;
	}

	public String toString(){
		return text;
	}
	@Override
	public void mouseClicked(int button, int arg1, int arg2, int arg3) {
		if(getMode() == Button.HOVER && Input.MOUSE_LEFT_BUTTON == button)
			fireEvent();
		
	}

	@Override
	public void mouseDragged(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseWheelMoved(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputEnded() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputStarted() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isAcceptingInput() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void setInput(Input arg0) {
		// TODO Auto-generated method stub
		
	}




	 


}


	






























