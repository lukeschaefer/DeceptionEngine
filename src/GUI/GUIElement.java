package GUI;
import java.util.ArrayList;
import java.util.Iterator;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.MouseListener;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;


public abstract class GUIElement implements MouseListener{
	int x;
	int y;
	int width;
	int height;
	Cell cell;
	
	GUILayout thisLayout;
	
	protected Shape clickBox = new Rectangle(0,0,0,0);
	protected int[] padding = new int[] {0,0,0,0};
	
	protected Color backgroundColor;
	
	public int getHeight() {
		// TODO Auto-generated method stub
		return height;
	}

	public int getWidth() {
		// TODO Auto-generated method stub
		return width;
	}

	public int getX() {
		// TODO Auto-generated method stub
		return x;
	}

	public int getY() {
		// TODO Auto-generated method stub
		return y;
	}
	
	public void setPadding(int top, int right, int bottom, int left){
		this.padding = new int[] {top,right,bottom,left};
	}
	
	public void setColor(Color color){
		this.backgroundColor = color;
	}
		
	 private ArrayList<Object> _listeners = new ArrayList<Object>();
	 
	  public synchronized void addEventListener(GUIListener listener)	{
	    _listeners.add(listener);
	  }
	  
	  public synchronized void removeEventListener(GUIListener listener)	{
	    _listeners.remove(listener);
	  }

	  protected synchronized void fireEvent()	{
		    GUIEvent event = new GUIEvent(this);
		    Iterator<Object> i = _listeners.iterator();
		    while(i.hasNext())	{
		      ((GUIListener) i.next()).guiActionPreformed(event);
		    }
		  }
	  
	int slotPosition[] = {0,0,1,1};
	abstract void drawGUI(Graphics g);
	
	public void updatePosition(){
		this.clickBox = new Rectangle(cell.x,cell.y,cell.width, cell.height);
	}
	
	
	
	public void snapTo(int x, int y) {
		this.slotPosition = new int[] {x,y,1,1};
		updatePosition();
		
	}

	public void putInCell(Cell cell2) {
		this.cell = cell2;
		this.updatePosition();
	}

	public void destroy() {
		return;
		
	}

	



	
	
}

