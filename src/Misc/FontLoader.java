package Misc;

import java.awt.Font;
import java.io.InputStream;

import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.util.ResourceLoader;


public class FontLoader {
	public static TrueTypeFont getFont(String fontPath, boolean aa, int size){
		    try {
		        InputStream inputStream = ResourceLoader.getResourceAsStream(fontPath);

		        Font awtFont2 = Font.createFont(Font.TRUETYPE_FONT, inputStream);
		        awtFont2 = awtFont2.deriveFont((float)size); // set font size
		        return(new TrueTypeFont(awtFont2, aa));

		    } catch (Exception e) {
		    	 Font awtFont = new Font("Times New Roman", Font.BOLD, size);
				 TrueTypeFont font = new TrueTypeFont(awtFont, aa);
				 return(font);
		    }   
		}
}
