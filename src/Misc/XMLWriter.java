package Misc;

import java.io.PrintStream;
import java.util.ArrayList;


public class XMLWriter {
	ArrayList<XMLNode>	nodes;
	XMLNode				defaultNode;
	
	public XMLWriter() {
		nodes = new ArrayList<XMLNode>();
	}
	
	public XMLWriter(String string) {
		nodes = new ArrayList<XMLNode>();
		defaultNode = new XMLNode(string);
		nodes.add(defaultNode);
	}
	
	public XMLNode addNode(String nodeName) {
		XMLNode newNode = new XMLNode(nodeName);
		defaultNode.addNode(newNode);
		return newNode;
	}
	
	
	public ArrayList<XMLNode> getNodesWith(String key, String value) {
		ArrayList<XMLNode> foundNodes = new ArrayList<XMLNode>();
		for (XMLNode node : nodes) {
			node.getNodesWith(key, value);
		}
		return foundNodes;
	}
	
	public ArrayList<XMLNode> getNodesWith(String name) {
		ArrayList<XMLNode> foundNodes = new ArrayList<XMLNode>();
		for (XMLNode node : nodes) {
			node.getNodesWith(name);
		}
		return foundNodes;
	}
	
	public void outputData(PrintStream out) {
		out.println("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		for (XMLNode node : nodes) {
			node.printNode(out, 0);
		}
		out.flush();
		out.close();
	}
}

class XMLNode {
	ArrayList<XMLKey>	keys;
	ArrayList<XMLNode>	subNodes;
	String				nodeName;
	
	public XMLNode(String nodeName) {
		this.nodeName = nodeName;
		keys = new ArrayList<XMLKey>();
		subNodes = new ArrayList<XMLNode>();
		
	}
	
	public void addNode(XMLNode newNode) {
		subNodes.add(newNode);
		
	}
	
	public void printNode(PrintStream out, int tabLevel) {
		out.println(repeat("\t", tabLevel) + "<" + nodeName + ">");
		for (XMLNode node : subNodes)
			node.printNode(out, (tabLevel + 1));
		for (XMLKey key : keys)
			out.println(repeat("\t", (tabLevel + 1)) + "<" + key.keyName + ">" + key.keyValue + "</" + key.keyName + ">");
		out.println(repeat("\t", tabLevel) + "</" + nodeName + ">");
		
	}
	
	public XMLNode addNode(String nodeName) {
		XMLNode newNode = new XMLNode(nodeName);
		subNodes.add(newNode);
		return newNode;
	}
	
	public void addKey(XMLKey key) {
		for (XMLKey checkKey : keys)
			if (checkKey.equals(key.keyName, key.keyValue)) {
				keys.remove(checkKey);
			}
		keys.add(key);
	}
	
	public void addKey(String name, String value) {
		for (XMLKey checkKey : keys)
			if (checkKey.equals(name, value)) {
				keys.remove(checkKey);
			}
		keys.add(new XMLKey(name, value));
	}
	
	public ArrayList<XMLNode> getNodesWith(String key, String value) {
		ArrayList<XMLNode> foundNodes = new ArrayList<XMLNode>();
		
		for (XMLKey xmlKey : keys)
			if (xmlKey.equals(key, value))
				foundNodes.add(this);
		for (XMLNode node : subNodes) {
			getNodesWith(key, value);
		}
		return foundNodes;
	}
	
	public ArrayList<XMLNode> getNodesWith(String name) {
		ArrayList<XMLNode> foundNodes = new ArrayList<XMLNode>();
		if (this.nodeName == name)
			foundNodes.add(this);
		for (XMLNode node : subNodes) {
			node.getNodesWith(name);
		}
		return foundNodes;
	}
	
	public String repeat(String str, int times) {
		StringBuilder ret = new StringBuilder();
		for (int i = 0; i < times; i++)
			ret.append(str);
		return ret.toString();
	}
	
	
}

class XMLKey {
	String	keyName;
	String	keyValue;
	
	public XMLKey(String keyName, String keyValue) {
		this.keyName = keyName;
		this.keyValue = keyValue;
	}
	
	
	public boolean equals(String key, String value) {
		if (this.keyName.equals(key) && this.keyValue.equals(value))
			return true;
		return false;
	}
}