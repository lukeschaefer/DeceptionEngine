package ObjectMaker;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;


import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class LayerMenu extends JPanel implements ActionListener{
	LayerList menu;
	Button add;
	Button delete;
	JTextField layerName;
	
	ArrayList<String> layers;
	
	LayerMenu(){
		super();
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.fill = gbc.BOTH;
		
		gbc.gridheight = 7;
		gbc.gridx = 1;
		gbc.weighty = 18;
		
		layers = new ArrayList<String>();
		for(int i = 0; i< 9; i++)
			layers.add("Layer "+(int)(Math.random()*1000));

		menu = new LayerList(layers);
		this.add(menu,gbc);
		this.setBackground(ColorTheme.shadow2);
		Border border = BorderFactory.createLineBorder(ColorTheme.shadow2,4);
		this.setBorder( border );
		
	

		
		gbc.gridheight = 1;
		gbc.gridy = 7;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.fill = gbc.BOTH;
		
		JPanel layerOptions = new JPanel();
		
		this.add(layerOptions, gbc);
		GridBagConstraints gbc2 = new GridBagConstraints();
		
			layerOptions.setLayout(new GridBagLayout());
			layerOptions.setPreferredSize(new Dimension(210,10));
			layerOptions.setBackground(ColorTheme.shadow2);
			
			add = new Button("addLayer.png", "Add Layer");
			add.addActionListener(this);		
			delete = new Button("deleteLayer.png", "Delete Layer");
			delete.addActionListener(this);
			
			gbc2.insets = new Insets(5,3,0,3);
			gbc2.anchor = gbc2.EAST;
			gbc2.gridx = 4;			
		
			layerOptions.add(add,gbc2);
			
			gbc2.gridx = 5;
	
			layerOptions.add(delete,gbc2);
			 
			gbc2.gridx = 1;
			gbc2.gridwidth = 3;
			gbc.fill = gbc.HORIZONTAL;
			
			layerName = new JTextField();
			layerName.setPreferredSize(new Dimension(200,16));
			layerName.setMinimumSize(new Dimension(150,18));
			layerName.setFont(layerName.getFont().deriveFont(10f));
			layerName.setBackground(ColorTheme.shadow);
			layerName.setForeground(ColorTheme.text);
			layerOptions.add(layerName, gbc2);

		this.setPreferredSize(new Dimension(210,200));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton){
			switch( ((JButton)e.getSource()).getText()  ){
			case "Add" :
				addLayer();
				break;
			case "Delete" :				
				delete();
				break;
			}
			
		}
		
	}

	private void addLayer() {
		String newLayer = "New Layer "+(int)(Math.random()*100);
		menu.layers.add(newLayer);
		menu.setSelected(newLayer);
		menu.repaint();
	}

	private void delete() {
		
		menu.setSelected(menu.layers.get(0));
		menu.layers.remove(this.menu.selected);

		menu.repaint();
	}

}
