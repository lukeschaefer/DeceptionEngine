package ObjectMaker;

public class ObjectLayer {
	String name;
	
	public ObjectLayer(String name){
		this.name = name;
	}
	
	public String toString(){
		return name;
	}
	
}
