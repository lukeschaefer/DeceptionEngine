package ObjectMaker;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;

import javax.swing.JPanel;

public class LayerList extends JPanel implements MouseMotionListener, MouseListener, MouseWheelListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5063021500755352764L;
	
	int mouseX;
	int mouseY;
	
	ArrayList<Object> layers;
	Object 			selected;
	
	public int layerHeight = 30;

	private int scrollbarWidth = 6;
	private int scrollBarHeight = 20;
	
	int scrollOffset;
	
	private boolean mouseInside;

	private int scrollSpeed = 10;

	
	
	
	
	public LayerList(ArrayList layers){
		this.layers = layers;
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addMouseWheelListener(this);
	
		
	}
	
	public void setSelected(Object o) {
		this.selected = o;
		repaint();		
	}
	
	public void paint(Graphics g){
		g.setColor(ColorTheme.background);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		
		int y = -scrollOffset;
		
		for(Object layer : layers){		
			if(layer == selected)
				g.setColor(ColorTheme.selected);
			else
			g.setColor( (new Rectangle(0,y,this.getWidth(),layerHeight).contains(mouseX,mouseY) && mouseInside)? ColorTheme.highlight : ColorTheme.shadow);
			
			g.fillRect(0,y,this.getWidth(),layerHeight-1);
			g.setColor(ColorTheme.shadow2);
			g.drawLine(0, y+layerHeight-2, this.getWidth(),  y+layerHeight-2);
			g.setColor(ColorTheme.text);					
			
			g.drawString(layer.toString(), 5, y+layerHeight/2);
			y+= layerHeight;				
			
		}
		
		if(mouseInside){
			if(layerHeight * layers.size() > this.getHeight()){
				g.setColor(ColorTheme.scrollBar);
				int position = (int)(((float)scrollOffset/((layers.size()*layerHeight)-this.getHeight()))*(this.getHeight()-scrollBarHeight));
				g.fillRect(this.getWidth() - scrollbarWidth - 5 , position,scrollbarWidth, scrollBarHeight);					
			}
		}			
	}
		
		
	@Override
	public void mouseClicked(MouseEvent arg0) {
		int y = -scrollOffset;		
		for(Object layer : layers){	
			if(new Rectangle(0,y,this.getWidth(),layerHeight-1).contains(mouseX,mouseY)){
				this.setSelected(layer);
				break;
			}
			y+= layerHeight;
		}
		
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		this.mouseInside = true;
		repaint();
	
		
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		this.mouseInside = false;
		repaint();
		
	}
	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseMoved(MouseEvent e) {
		mouseX = e.getX();
		mouseY = e.getY();
		repaint();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		this.scrollOffset += e.getWheelRotation()*scrollSpeed;
		
		if(scrollOffset < 0)
			scrollOffset = 0;
		else if (scrollOffset > (layers.size()*layerHeight)-this.getHeight())
			scrollOffset = (layers.size()*layerHeight)-this.getHeight();
		else
			repaint();
		
	}
}


