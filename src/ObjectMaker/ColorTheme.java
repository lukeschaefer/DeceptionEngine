package ObjectMaker;

import java.awt.Color;

public class ColorTheme {
	
	public static Color background = 	new Color(93,90,90);	
	public static Color highlight =  	new Color(220,180,120);	
	public static Color selected = 		new Color(220,130,10);
	
	
	public static Color shadow = 	 	new Color(70,70,70);
	public static Color shadow2 = 	 	new Color(30,30,30);
	
	public static Color focus = 	 	new Color(90,100,100);
	public static Color viewportBackground = new Color(20,15,10);
	public static Color text = 			new Color(225,255,255);
	public static Color scrollBar =		new Color(0,0,0,90);

	public static Color buttonNormal = 	new Color(70,70,70);
	
}
