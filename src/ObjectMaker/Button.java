package ObjectMaker;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JPanel;

public class Button extends JPanel{
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 2707502926755133017L;
	
	private ArrayList<ActionListener> listeners;
	private BufferedImage image;
	private String		  name;	
	
	public Button(String fileName, String name){
		super();
		listeners = new ArrayList<ActionListener>();
		
		this.setPreferredSize(new Dimension(20,15));
		this.setMinimumSize(new Dimension(20,15));
		try {
			image = ImageIO.read(new File("images/ObjectMaker/"+fileName));
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		this.name = name;
	}
	
	public void setImage(String fileName){
	
	}
	
	public void paint(Graphics g){
		g.setColor(ColorTheme.buttonNormal);
		g.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), 8, 8);

		g.drawImage(image,
				// Destination coordinates
				(this.getWidth()/2) - (image.getWidth()/2) , 	(this.getHeight()/2) - (image.getHeight()/2), 				 
				(image.getWidth()),		image.getHeight(),  
				0, 0, image.getWidth(), image.getHeight(),null);
	}

	public void addActionListener(ActionListener listener) {
		listeners.add(listener);
	}

}
