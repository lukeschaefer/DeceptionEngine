package ObjectMaker;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Label;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class ObjectMaker {
	static JFrame window;
	static Label bottomWarning;
	public static void main(String args[]){
		
		 try {
		        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		        UIManager.put("List.lockToPositionOnScroll", Boolean.FALSE);
		    } catch (Exception e) { }
		 
		 bottomWarning = new Label("                                                                                                                                               ");
		window = new JFrame();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLayout(new BorderLayout());
		
		JPanel bottomWarning = new JPanel();
		bottomWarning.setPreferredSize(new Dimension(30,35));
		bottomWarning.add(ObjectMaker.bottomWarning);
		window.add(bottomWarning, BorderLayout.SOUTH);
	 
		
		JPanel eastPanel = new JPanel();
		eastPanel.setBackground(ColorTheme.background);	
		eastPanel.setPreferredSize(new Dimension(220,400));
		eastPanel.add(new LayerMenu());
		window.add(eastPanel, BorderLayout.EAST);
		
		JPanel viewPort = new JPanel();
		viewPort.setBackground(ColorTheme.viewportBackground);
		window.add(viewPort, BorderLayout.CENTER);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(new JMenuItem("File"));
		window.setJMenuBar(menuBar);	
		
		window.setSize(900,700);
		window.show();
		
		ObjectMaker.println("Started Object Maker");
	}
	
	public static void println(String s){
		bottomWarning.setPreferredSize(new Dimension(500,30));
		bottomWarning.setText(s);
		bottomWarning.repaint();
	}
}
