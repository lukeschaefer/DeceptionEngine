package Game;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import Engine.Flicker;
import Engine.Light;
import Misc.FontLoader;


public class LoadingState extends BasicGameState{
	private Image loadingImage;
	private Font loadingFont;
	LoadAssetsThread lat;
	
	public static int ID = 3;
	
	PlayGameState pgs;
	private int levelNumber;
	private boolean finished = false;
	
	public LoadingState(int lOADINGSTATE, int levelNumber) {
		ID = lOADINGSTATE;
	}

	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		this.loadingImage = new Image("images/loading.png");
		loadingFont = FontLoader.getFont("fonts/missionscript.ttf", true, 48);		
	}

	public void enter(GameContainer gc, StateBasedGame game){
		System.out.println("ENTERED STATE");
		
	    game.enterState(DeceptionGame.LEVELSTATE);
		//pgs = (PlayGameState) game.getState(DeceptionGame.LEVELSTATE);
		//lat = new LoadAssetsThread(pgs, this);
	}

	
	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {
		g.setBackground(Color.black);
		int x = gc.getWidth()/2-loadingImage.getWidth()/2;
		int y = gc.getHeight()/2-loadingImage.getHeight()/2;
		g.drawImage(loadingImage, x, y);
		g.setFont(loadingFont);
		g.setColor(Color.blue);
		//g.drawString(Integer.toString(lat.completed),gc.getWidth()/2-loadingFont.getWidth(Integer.toString(lat.completed))/2, y+300);
		
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {		
		loadingImage.rotate(-delta/4f);
		if(finished){	
			// TODO: Switch to Engine.initLists();
			pgs.initLists();
			System.out.println("Finished loading.");
			game.enterState(DeceptionGame.LEVELSTATE);
		}
		
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return ID;
	}

	public void finish() {
		this.finished  = true;
		

	}

}

class LoadAssetsThread extends Thread{
	String fileName;
	ArrayList<Light> lightList;
	PlayGameState state;
	LoadingState lState;
	int completed;
	
	public LoadAssetsThread(PlayGameState pgs, LoadingState loadingState){
		fileName = "Levels/Level "+pgs.getLevelNumber()+"/";
		state = pgs;
		lState = loadingState;
		this.completed = 0;		
		this.start();
	}
	
	public void run(){
		loadLights();
		giveStateAssets();
		finish();
		
	}

	private void finish() {
		lState.finish();
		
	}

	private void giveStateAssets() {
		state.lightList = lightList;	
	}

	private void loadLights() {
		
		File lightFile = new File(fileName+"lights.txt");
		BufferedReader in;
		String line;		
		lightList = new ArrayList<Light>();
		
		
		try{
			in = new BufferedReader(new FileReader(lightFile));
			System.out.println("Getting light file from "+lightFile.getAbsolutePath());
			//Light light = new Light(50,50,250);
			while ((line = in.readLine()) != null) {
				completed++;
				// If new light:
				Light newLight;
				
			    if(line.matches("light\\.\\d+")){
			    	System.out.println("creating new light.");
			    	newLight = new Light(1,1,1);			    
			    	
			    	// set x:
			    	line = in.readLine();
			    	if(line.matches("\\tx\\.\\d+"))		    	
					    	newLight.setX(Integer.parseInt(line.replaceAll( "\\D", "")));		    	
			    	else{
			    		System.out.println("Invalid light file.");
			    		continue;
			    	}
			    	// set y:
			    	line = in.readLine();
			    	if(line.matches("\\ty\\.\\d+"))    	
					    	newLight.setY(Integer.parseInt(line.replaceAll( "\\D", "")));		
					
			    	// TODO: Set Color;
			    	// TODO: Set Flicker;			    	
			    	
			    		    	
				   lightList.add(newLight);
				   newLight.setColor(new Color((float) Math.random(),(float) Math.random(), (float) Math.random()));
				   //newLight.setFlicker(flicker);
				   newLight.setBrightness(90);
				   newLight.setRadius(200);
			    	System.out.println("\tAdding light.");
			    	
			    }
			    
			}
			in.close();
		}catch(Exception e){
			
			System.err.print("Error parsing light file:\n\t");
			e.printStackTrace();
			return;			
		}
		

		
	}
	
}











