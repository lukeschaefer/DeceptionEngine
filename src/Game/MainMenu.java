package Game;

import GUI.*;

import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class MainMenu extends BasicGameState implements GUIListener {
	GUILayout mainMenu;
	GameContainer gc;
	StateBasedGame game;
	private int id;
	
	public MainMenu(int state) {
		this.id = state;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		this.game = game;
		int menuWidth = container.getWidth()/7;
		this.gc = container;
		mainMenu = new GUILayout((container.getWidth()/2)-menuWidth/2, 100, menuWidth, 800, 1, 6, this);
		mainMenu.add(new Button("Start Game"),1,1);
		mainMenu.add(new Button("Credits"),1,2);
		mainMenu.add(new Button("Fullscreen"),1,3);
		mainMenu.add(new Button("Quit"),1,4);
		gc.setTargetFrameRate(60);
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		mainMenu.draw(g);		
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)	throws SlickException {

	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return id;
	}


	@Override
	public Input getInput() {
		// TODO Auto-generated method stub
		return gc.getInput();
	}

	@Override
	public void guiActionPreformed(GUIEvent e) {
		if(isAcceptingInput()){
			if(e.getSource().toString() == "Quit")
				gc.exit();
			if(e.getSource().toString() == "Start Game"){

				this.getInput().removeAllMouseListeners();
				game.enterState(DeceptionGame.LOADINGSTATE);
			}	
		}else{
			System.out.println("DIS NIGGA BE CLOSED!");
		}
	}

	@Override
	public void leave(GameContainer gc, StateBasedGame g){
		
	}
	





}
