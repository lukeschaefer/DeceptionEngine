package Game;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import Engine.GameObject;



public class Player extends GameObject{

	int frameDuration = 100;
	
	int speed = 400;
	double friction = .85;
	int maxSpeed = 500;
	
	Image still;
	
	boolean UP;
	boolean DOWN;
	boolean LEFT;
	boolean RIGHT;
	
	private Animation left;
	private Animation right;
	private Animation up;
	private Animation down;
	
	private int width;
	private int height;
	private int direction;
	private Input input;
	
	Player(Input input) throws SlickException{
		this.input = input;		
		getAnimations(new SpriteSheet(new Image("images/sprites/player.png"),51,72));
		width = 100;
		height = 120;
	}


	private void getAnimations(SpriteSheet spriteSheet) {
		left = new Animation(true);
		right = new Animation(true);
		up = new Animation(true);
		down = new Animation(true);
		
		
		int y=0;
		for(int x = 0; x < 8; x++){
			down.addFrame(spriteSheet.getSprite(x,y),frameDuration);
		}
		y++;
		for(int x = 0; x < 8; x++){
			left.addFrame(spriteSheet.getSprite(x,y),frameDuration);
		}
		y++;
		for(int x = 0; x < 8; x++){
			right.addFrame(spriteSheet.getSprite(x,y),frameDuration);
		}
		y++;
		for(int x = 0; x < 8; x++){
			up.addFrame(spriteSheet.getSprite(x,y),frameDuration);
		}
		
		
	}


	@Override
	public void draw(Graphics g) throws SlickException {
		if(UP)
			g.drawAnimation(up,x,y);
		else if(DOWN)
			g.drawAnimation(down,x,y);
		else if(LEFT)
			g.drawAnimation(left,x,y);
		else
			g.drawAnimation(right,x,y);
	}

	private int getHeight() {
		// TODO Auto-generated method stub
		return this.height;
	}


	private int getWidth() {
		// TODO Auto-generated method stub
		return this.width;
	}


	@Override
	public void update(float dt) {

		RIGHT = false;
		LEFT = false;
		UP = false;
		DOWN = false;
		
		if(input.isKeyDown(Input.KEY_RIGHT))
			RIGHT = true;
		if(input.isKeyDown(Input.KEY_LEFT))
			LEFT = true;
		if(input.isKeyDown(Input.KEY_UP))
			UP = true;
		if(input.isKeyDown(Input.KEY_DOWN))
			DOWN = true;
		
		if(LEFT)
			mx = -100;
		else if(RIGHT)
			mx = 100;
		else 
			mx = 0;
		
		if(UP)
			my = -100;
		else if(DOWN)
			my = 100;
		else 
			my = 0;
		
		this.x += mx*dt;
		this.y += my*dt;
		
	}
}
