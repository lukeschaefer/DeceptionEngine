package Game;

import java.io.IOException;
import java.util.ArrayList;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.ImageBuffer;
import org.newdawn.slick.Input;
import org.newdawn.slick.MouseListener;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import Engine.GameObject;
import Engine.Light;
import Engine.ShadowCaster;

import org.newdawn.slick.geom.Shape;

public class PlayGameState extends BasicGameState{
	
	private int		id	= 2;
	
	private float	x	= 100;
	
	Image			backgroundImage;
	StateBasedGame	game;
	GameContainer	gc;
	
	Player			player;
	
	ArrayList<Light> lightList;
	ArrayList<GameObject> objectList;
	
	Image		shadowBuff;

	private Graphics shadowG;
	
	public PlayGameState(int id) {
		this.id = id;
	
	}
	
	public void pauseGame() {
		System.out.println("Pausing!");
		try {
			((PauseState) game.getState(DeceptionGame.PAUSESTATE)).initPause(this);
		} catch (SlickException e) {
			e.printStackTrace();
		}
		//gc.setPaused(true);
		game.enterState(DeceptionGame.PAUSESTATE);
		
		
	}
	
	@Override
	public int getID() {
		return id;
	}
	
	public void enter(GameContainer gc, StateBasedGame game){
		gc.getInput().addMouseListener(this);
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame game) throws SlickException {
		lightList = new ArrayList<Light>();
		objectList = new ArrayList<GameObject>();
		
		shadowBuff = new Image(gc.getWidth(), gc.getHeight());
		shadowG = shadowBuff.getGraphics();
		
		GameRect rect = new GameRect(200,200,50,70);
		GameRect rect1 = new GameRect(300,210,50,50);
		GameRect rect2 = new GameRect(240,400,100,50);
		objectList.add(rect);
		objectList.add(rect1);
		objectList.add(rect2);
		
		Light mouseLight = new Light(200,200,200);
		lightList.add(mouseLight);
		this.backgroundImage = new Image("images/Level 1/mapImage.png");
		this.gc = gc;
		this.game = game;
		System.out.println("OpenGL version: " + GL11.glGetString(GL11.GL_VERSION));
		
		initLists();
	}
	
	@Override
	public void update(GameContainer container, StateBasedGame game, int milli) throws SlickException {
		float delta = (float) (milli / 1000.0);
		x += delta * 20;

		
		shadowG.setColor(Color.red);
		for(GameObject obj : objectList){
			if(obj instanceof ShadowCaster){
				Shape shape = ((ShadowCaster) obj).getShadowShape();
				float[] points = shape.getPoints();
				int i = shape.getPointCount()-1;
				int j = 0;
				do{
					int normal =  ((int) (((Math.atan2( points[j*2+1] -  points[i*2+1], points[i*2] -  points[j*2]))/(Math.PI*2))*256) + 512 - 128) % 256;
					shadowG.setColor(new Color(normal, 50, 255-normal));
		
					shadowG.drawLine(points[i*2], points[i*2+1], points[j*2], points[j*2+1]);	
					
//					 debug arrow
//					shadowG.setColor(Color.white);
					
//					double dx = Math.sin(  (normal/256f)*Math.PI*2);
//					double dy = Math.cos(  (normal/256f)*Math.PI*2);
//					int startX = (int) ((points[i*2] + points[j*2])/2);
//					int startY = (int) ((points[i*2+1] + points[j*2+1])/2);
					
//					shadowG.drawLine(startX, startY, (int) (startX+dx*20 ), (int) (startY+dy*20));
					
					j = i;
					i--;					
				}while(i >= 0);
				
//				for(i = 0; i < shape.getPointCount(); i++){
//					float[] normal = shape.getNormal(i);
//					int normalInt = (int) (((Math.atan2(normal[0], normal[1])/(Math.PI*2)))*256 + 512-64) % 256;
//					shadowG.setColor(new Color(normalInt, 50, 255-normalInt));
//					float[] dot = shape.getPoint(i);
//					shadowG.drawLine(dot[0]-1, dot[1], dot[0], dot[1]);
//				}
				
				
			}
		}		
		shadowG.flush();

		
		for(Light light : lightList)
			light.update(delta, shadowBuff.getTexture());
	}
	
	
	@Override
	public void render(GameContainer arg0, StateBasedGame arg1, Graphics g) throws SlickException {	
		for(Light light : lightList){
			light.draw(g);
		}
		for(GameObject obj : objectList){
			obj.draw(g);
		}
//		g.drawImage(shadowBuff, 0, 0);
	}
	
	@Override
	public void keyReleased(int key, char c) {
		if (key == Input.KEY_ESCAPE)
			pauseGame();
	}

	public int getLevelNumber() {
		return 1;
	}

	public void initLists() {	
		System.out.println("Instantiating " + lightList.size() + " lights");
		for(Light light : lightList)
			light.init();
	}
		
	
	public void mouseMoved(int p, int j, int x, int y){
		if(lightList.size() > 0){
			Light light = lightList.get(0);
			light.x = x;
			light.y = y;
		}		
	}
	
}

class GameRect extends GameObject implements ShadowCaster{
	
	private int width;
	private int height;

	public GameRect(int x,int y,int width,int height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;		
	}	


	@Override
	public Shape getShadowShape() {
		return (Shape) new Rectangle(x,y,width,height);
	}

	@Override
	public void draw(Graphics g) throws SlickException {
		Color c = g.getColor();
		g.setColor(Color.gray);
		g.fillRect(x, y, width, height);	
		g.setColor(c);
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
	}
}
