package Game;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;


public class Zone{
	public Shape wallBox;
	Zone(Shape shape){
		this.wallBox = shape;
	}
	
	public void setShape(Shape shape){
		wallBox = shape;
	}
	public void draw(Graphics g) {
		//g.setDrawMode(Graphics.MODE_ADD);
		g.setColor(new Color(255,255,255,100));
		g.fill(wallBox);
		g.setDrawMode(Graphics.MODE_NORMAL);
	}

}
