package Game;
import org.newdawn.slick.AppGameContainer; 
import org.newdawn.slick.GameContainer; 
import org.newdawn.slick.SlickException; 
import org.newdawn.slick.state.StateBasedGame; 


public class DeceptionGame extends StateBasedGame { 	
	public static GameContainer gc;
	
	public static int MAINMENUSTATE = 1;
	public static int LEVELSTATE = 2;
	public static int PAUSESTATE = 3;
	public static int LOADINGSTATE = 4;
	
	public int currentState = 1;

    public DeceptionGame() { 
        super("State Based Test");         
        this.addState(new MainMenu(MAINMENUSTATE));
        this.addState(new PlayGameState(LEVELSTATE));
        this.addState(new PauseState(PAUSESTATE));
        this.addState(new LoadingState(LOADINGSTATE,1));
        this.enterState(MAINMENUSTATE);    
    } 
     
    /** 
     * @see org.newdawn.slick.state.StateBasedGame#initStatesList(org.newdawn.slick.GameContainer) 
     */ 
    public void initStatesList(GameContainer container) throws SlickException{ 
        this.getState(MAINMENUSTATE).init(container, this);
//        this.getState(LEVELSTATE).init(container, this); 
        this.getState(LOADINGSTATE).init(container, this); 
    } 
     
    /** 
     * Entry point to our test 
     *  
     * @param argv The arguments to pass into the test 
     */ 
    public static void main(String[] argv) { 
        try { 
            AppGameContainer container = new AppGameContainer(new DeceptionGame()); 
            container.setDisplayMode(container.getScreenWidth()/2,container.getScreenHeight()*2/3,false); 
            container.start(); 
            
        } catch (SlickException e) { 
            e.printStackTrace(); 
        } 
    }

	public static GameContainer getGc() {
		return gc;
	}

	public void setGc(GameContainer gc) {
		this.gc = gc;
	} 
} 