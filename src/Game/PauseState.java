package Game;

import org.lwjgl.Sys;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.opengl.shader.ShaderProgram;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import GUI.Button;
import GUI.GUIEvent;
import GUI.GUILayout;
import GUI.GUIListener;


public class PauseState extends BasicGameState implements GUIListener {
	
	int						id;
	GUILayout				pauseMenu;
	GameContainer			gc;
	private PlayGameState	backgroundState;
	private Image			pauseBackground;
	private Image			pauseOverlay;
	private StateBasedGame	game;
	
	
	public PauseState(int id) {
		this.id = id;
	}
	
	
	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {
		g.drawImage(pauseBackground, 0, 0);
		
		
		g.texture(new Rectangle(0, 0, gc.getWidth(), gc.getHeight()), pauseOverlay, .0025f, .0025f);
		g.setDrawMode(Graphics.MODE_NORMAL);
		pauseMenu.draw(g);
		
	}
	
	@Override
	public void update(GameContainer arg0, StateBasedGame arg1, int arg2)
			throws SlickException {
		
		
	}
	
	
	@Override
	public void init(GameContainer gc, StateBasedGame game) throws SlickException {
		this.gc = gc;
		this.game = game;
	}
	
	
	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return id;
	}
	
	
	public void initPause(PlayGameState state) throws SlickException {
		int menuWidth = gc.getWidth() / 5;
		int menuHeight = gc.getHeight() - gc.getHeight() / 4;
		
		pauseMenu = new GUILayout(gc.getWidth() / 2 - menuWidth / 2, gc.getHeight() / 2 - menuHeight / 2, menuWidth, menuHeight, 1, 5, this);
		pauseMenu.add(new Button("Continue"), 1, 1);
		pauseMenu.add(new Button("Exit Game"), 1, 2);
		pauseOverlay = new Image("images/pauseBackground.png");
		pauseOverlay.setAlpha(.1f);
		
		this.backgroundState = state;
		this.pauseBackground = blurImage(state.gc.getGraphics());
	}
	
	
	public Image blurImage(Graphics g) throws SlickException {
		
		Image newBackground = new Image(gc.getWidth(), gc.getHeight());
		g.copyArea(newBackground, 0, 0);
		Graphics gBlur = newBackground.getGraphics();
		//gBlur.clear();
		
		
		if (!ShaderProgram.isSupported()) {
			Sys.alert("Error", "Your graphics card doesn't support OpenGL shaders.");
		} else {
			// load our vertex and fragment shaders
			String hBlur = "images/shaders/blur/blurH.frag";
			String vBlur = "images/shaders/blur/blurV.frag";
			String vert = "images/shaders/blur/blur.vert";
			
			ShaderProgram blurHoriz = ShaderProgram.loadProgram(vert, hBlur);
			ShaderProgram blurVert = ShaderProgram.loadProgram(vert, vBlur);
			
			gBlur.setDrawMode(Graphics.MODE_NORMAL);
			
			blurVert.bind();
			blurVert.setUniform1f("blurSize", (float) (1.0 / gc.getHeight()));
			blurHoriz.bind();
			blurHoriz.setUniform1f("blurSize", (float) (2.0 / gc.getWidth()));
			
			gBlur.drawImage(newBackground, 0, 0);
			blurHoriz.unbind();
			ShaderProgram.unbindAll();
			
			gBlur.flush();
			gBlur.destroy();
			
			return newBackground;
		}
		return null;
	}
	
	
	@Override
	public Input getInput() {
		return gc.getInput();
	}
	
	
	@Override
	public void guiActionPreformed(GUIEvent e) {
		String button = ((Button) e.getSource()).getText();
		if (button == "Continue") {
			game.enterState(backgroundState.getID());
			pauseMenu.removeListeners();
		}
		
		
	}
	
	
}
