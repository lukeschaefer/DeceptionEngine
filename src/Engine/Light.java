package Engine;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.ImageBuffer;
import org.newdawn.slick.ShapeFill;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.opengl.shader.ShaderProgram;
import org.newdawn.slick.svg.Gradient;
import org.newdawn.slick.svg.RadialGradientFill;

public class Light extends GameObject{

	private Color color;
	private ShaderProgram sp;
	
	private int radius = 600;
	private int brightness = 150;
    
	private Flicker brightnessFlicker;
	private ImageBuffer buffer;
    
	int pixelScale = 8;
	int photonScale = 1;
	
	public void init(){
		
	}
	
	public void setFlicker(Flicker flicker2){
		this.brightnessFlicker = flicker2;
	}
	
	
	public Light(float x, float y, int radius){
		this.x = x;
		this.y = y;   		
		this.color = new Color(255,255,255);  
		this.brightnessFlicker = null;
		buffer = new ImageBuffer(960>>1,720>>1);
	}
	
	public void setBrightness(int b){
		this.brightness = b;
	}


	public void draw(Graphics g){	
		Image scaled = buffer.getImage();
		scaled.setFilter(Image.FILTER_LINEAR);

		scaled = scaled.getScaledCopy(pixelScale);
		g.drawImage(scaled, 0, 0);
		
	}
	
	

//	private void getShadowList(List<Square> walls) {
//		Graphics shadowGraphics;
//		try {
//			shadowsImage = new Image(600,600);
//			shadowGraphics = shadowsImage.getGraphics();
//			shadowGraphics.setBackground(Color.white);
//			shadowGraphics.setColor(Color.black);
//			shadowGraphics.setAntiAlias(true);
//			for(Square thisSquare : walls){
//				Polygon[] shadows = thisSquare.getShadows(getX(), getY());
//				for(int i = 0; i<4; i++)
//					shadowGraphics.fill(shadows[i]);
//				shadowGraphics.flush();
//			}
//		} catch (SlickException e) {
//			e.printStackTrace();
//		}
//	}

	
//	public static int getLightAmountFor(Object obj) {
//		int totalLight = 5;
//		else{
//			for(Light light : GameEngine.getLightList()){
//				if(light.hasLOS(obj))
//					totalLight += Math.max(600-light.getDistanceTo(obj),0)/30;
//			}
//		}
//		return totalLight;
//	}



	public void update(float dt, Texture texture) {
		if(brightnessFlicker != null){
			brightnessFlicker.update(dt);
		}

		buffer = new ImageBuffer(960/pixelScale,720/pixelScale);
		
		byte[] pixels = buffer.getRGBA();
		
		byte brightness =  (byte) (128 +  (byte) (128/pixelScale));
		for(int i = 0; i < pixels.length; i+=4){
			pixels[i] = (byte) 255;
			pixels[i+1] = (byte) 255;
			pixels[i+2] = (byte) 255;
			pixels[i+3] = (byte) 0;
			
		}
	
//		g.setColor( new Color(test,test,test));
		
		Stack<Photon> photons = new Stack<Photon>();
		
		byte[] shadowData = texture.getTextureData();
		
		for(int i = 0; i < this.radius*photonScale; i++){		
			
			float photon_dx = (float) ( Math.sin(((float)i/(this.radius/(2*photonScale)))*Math.PI ));
			float photon_dy = (float) (Math.cos(((float)i/(this.radius/(2*photonScale)))*Math.PI ));
			float photon_x  =  this.x;
			float photon_y  = this.y;
			
			photons.push(new Photon(photon_x, photon_y, photon_dx, photon_dy, 4));
		}
		
		Photon photon;
		int i = 0;
		while(!photons.isEmpty()){		

			photon = photons.pop();
			i++;
			
			for(int j = 0; j < this.radius; j++){
				
				if(photon.x < 0 || photon.x > 959 || photon.y<0 || photon.y>719)
					break;
				
				int shadowIndex = (int) ((Math.round((photon.x)) + (Math.round(photon.y)) * 1024)*4);
				// r > rgb impact
				// g > roughness
				// b > b - reflect
				// a > transparency
				
				if(shadowData[shadowIndex+1] > 0){
					if(photon.a <= 2)
						break;
					float AOI = (float)( Math.atan2(photon.dy, photon.dx));
					float wall = (float) ((float) ((shadowData[shadowIndex]/256f)*Math.PI*2));
					float bounce = wall-AOI;
					bounce = bounce + wall;
					bounce = (bounce+wall)/2;
					for(int k = 0; k < 20; k++){						
						photon.dy = (float) Math.sin(  Math.PI/2 +  wall - (k-10)*Math.PI/20 );
						photon.dx = (float) Math.cos(  Math.PI/2 + wall - (k-10)*Math.PI/20  );
						
					
						
						photons.push(new Photon(photon.x + photon.dx, photon.y+photon.dy, photon.dx, photon.dy, 2));
						
					}
					break;
				}

		
				int index = (int) (3 + (Math.round((photon.x/pixelScale)) + (Math.round(photon.y/pixelScale)) * 1024/pixelScale)*4);
				
//				if(((int)(photon.x * photon.y % (j + i)))%150 == 130)
//					photon.a--;
				if(photon.a <= 1)
					break;
				int currentLight = pixels[index];
				int nextLight = currentLight + photon.a/2;
				if(currentLight < 0 && nextLight >= 0)
					nextLight = -1;
				
				pixels[index] =  (byte) nextLight;
		
				photon.x += photon.dx;
				photon.y += photon.dy;
			}
			
		}		

		
		
	}


	public void setColor(Color color2) {
		this.color = color2;
		
	}


	public void setRadius(int i) {
		this.radius = i;
		
	}


	public Color getColor() {
		// TODO Auto-generated method stub
		return this.color;
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
	}
}

class Photon{
	public float x;
	public float y;
	public float dx;
	public float dy;
	
	public int a;
	
	Photon(float x, float y, float dx, float dy){
		this.x = x;
		this.y = y;
		this.dx = dx;
		this.dy = dy;
		this.a = 1;
	}
	
	
	Photon(float x, float y, float dx, float dy, int a){
		this.x = x;
		this.y = y;
		this.dx = dx;
		this.dy = dy;
		this.a = a;
	}
	
	

}
