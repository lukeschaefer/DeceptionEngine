package Engine;
import java.util.ArrayList;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.GameState;


public abstract class GameObject
{
	protected GameState game;
	public float x;
	public float y;
	
	protected float mx;
	protected float my;
	
	public float getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public float getMx() {
		return mx;
	}

	public void setMx(float mx) {
		this.mx = mx;
	}

	public float getMy() {
		return my;
	}

	public void setMy(float my) {
		this.my = my;
	}
	
	public void setPos(int x, int y){
		this.x=x;
		this.y=y;
	
	}

	public Shape getCollisionBox() {
		return collisionBox;
	}

	public void setCollisionBox(Shape collisionBox) {
		this.collisionBox = collisionBox;
	}


	
	Shape collisionBox;
	
	public abstract void draw(Graphics g) throws SlickException;
	
	public abstract void update(float dt);
	
	public boolean isTouching(GameObject target){
		if(this.getCollisionBox().intersects(target.getCollisionBox()) || this.getCollisionBox().contains(target.getCollisionBox()))
			return true;
		return false;
	}
	
	public boolean isInside(GameObject target){
		if(target.getCollisionBox().contains(this.getCollisionBox()))
			return true;
		return false;
	}
	
	public float getDistanceTo(GameObject target){
		return (float) Math.sqrt(Math.pow(this.getX()-target.getX(),2)+Math.pow(this.getY()-target.getY(), 2));
	}
	
	public float getDistanceTo(float x, float y){
		return (float) Math.sqrt(Math.pow(this.getX()-x,2)+Math.pow(this.getY()-y, 2));
	}
	
	public boolean hasLOS(GameObject target, ArrayList<Shape> blockers){
		float distance = getDistanceTo(target);
		float mx = (-getX()+target.getX())/distance;
		float my = (-getY()+target.getY())/distance;
		for(int i = 0; i<distance; i+=5){
			for(Shape shape : blockers)
				if(shape.contains(mx*i,my*i))
						return false;
		}
		return true;
		
	}
	
	
	
	
	
}