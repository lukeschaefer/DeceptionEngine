package Engine;

import org.newdawn.slick.Color;
import org.newdawn.slick.ShapeFill;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

public class LightFill implements ShapeFill{

	@Override
	public Color colorAt(Shape shape, float x, float y) {
		x -= shape.getX();
		y -= shape.getY();
		

		if(x > 3)		
			return Color.blue;
		return Color.red;
	}

	@Override
	public Vector2f getOffsetAt(Shape shape, float x, float y) {
		// TODO Auto-generated method stub
		return new Vector2f(0.0f,0.0f);
	}

}
