package Engine;
import org.newdawn.slick.geom.Shape;


public interface ShadowCaster {
	Shape getShadowShape();
}
