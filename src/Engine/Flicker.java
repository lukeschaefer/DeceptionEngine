package Engine;

import org.newdawn.slick.Color;

public class Flicker {
	
	int baseNum;
	
	float position = 0;
		
	String flickerString;	// Flicker for all channels, for brightness flicker;
	int influence = 1;	// Influence, how much the flicker effects the color output. 5 is probably a decent point.
	
	int flickerSpeed = 1;	// Flickers per second.
	int speedWiggle;	// Randomness
	

	// TODO: Make the flicker not be a string, but like - a queue of bytes or something.
	/**
	 * @param num base brightness. 1-255 maybe?
	 * @param string The string through which the brightness will iterate.
	 */
	public Flicker(int base, String string){
		this.baseNum = base;
		this.flickerString = string;
		
	}
	
	/**
	 * @param num base brightness. 1-255 maybe?
	 * @param string The string through which the brightness will iterate.
	 * @param speed The speed that it will cycle through the flicker. Defaults to one.
	 */
	public Flicker(int base, String string, int speed) {
		this.baseNum = base;
		this.flickerString = string;
		this.flickerSpeed = speed;
	}

	public int getValue(){
		int position = (int) this.position;
		
		// I have literally no idea if this works the way i hope it should.
		int uniformChange =   (int) (flickerString.charAt(position))- (int)'m';

		return baseNum+(uniformChange*influence);
	}
	
	public void update(float dt){
		position += dt*flickerSpeed;		
		// reset if position is further than the length of the flicker pattern
		if(position >= flickerString.length())
			this.position = 0;
	
		
	}
	
	public String getFlicker() {
		return flickerString;
	}
	
	public void setFlicker(String uniformFlicker) {
		this.flickerString = uniformFlicker;
	}
	

	
	public int getFlickerSpeed() {
		return flickerSpeed;
	}
	
	public void setFlickerSpeed(int flickerSpeed) {
		this.flickerSpeed = flickerSpeed;
	}
	
	public int getSpeedWiggle() {
		return speedWiggle;
	}
	
	public void setSpeedWiggle(int speedWiggle) {
		this.speedWiggle = speedWiggle;
	}

	public void setInfluence(int i) {
		this.influence = i;
		
	}
	

	
	
}
